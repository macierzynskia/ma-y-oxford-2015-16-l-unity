﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OneLivePanelController : MonoBehaviour {

    public GameObject ticketPanel;
    public GameObject solutionPanel;
    public GameObject instructionPanel;
    public GameObject problemPanel;
    public GameObject playingPanel;
    public GameObject notePanel;

    List<GameObject> panelsList = new List<GameObject>();

    	// Use this for initialization
	void Start () {
        panelsList.Add(ticketPanel);
        panelsList.Add(solutionPanel);
        panelsList.Add(instructionPanel);
        panelsList.Add(problemPanel);
        panelsList.Add(playingPanel);
        panelsList.Add(notePanel);
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void UpdatePanelsActivness() {

    }
}
