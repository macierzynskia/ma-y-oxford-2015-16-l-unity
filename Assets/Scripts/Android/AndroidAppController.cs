﻿using UnityEngine;
using System.Collections;

public class AndroidAppController : MonoBehaviour {

    // Use this for initialization
    AndroidJavaClass jc;
    AndroidJavaObject jo;

    void Start () {
        var androidJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        jc = new AndroidJavaClass("fr.pchab.androidrtc.RtcActivity");
        jo = androidJC.GetStatic<AndroidJavaObject>("currentActivity");
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Click() {
        jc.CallStatic("Launch", jo);
    }
}
