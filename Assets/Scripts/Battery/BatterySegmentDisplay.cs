﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
namespace AugmentedReallityApp
{
    public class BatterySegmentDisplay : MonoBehaviour
    {
        private const string ERROR_VOLTAGE_MESSAGE = "ERROR";
        private const string PERCENT_SIGN = " %";
        private const string PERCENT_VOLTAGE_FORMAT = "0.00";
        private const string ALARM_MARK = " !";

        public Text     sourceBatteryPanel;
        public Text     displayTextField;
        public float    minBatteryVoltage;
        public float    maxBatteryVoltage;
        public float    alarmVoltageLvl;

        private float currentPercentVoltage;

        void Update()
        {
            UpdateBatteryStatus();
        }

        private void UpdateBatteryStatus()
        {
            string strVoltage = GetStrVoltage();
            float voltage = ParseVoltage(strVoltage);
            if (float.IsNaN(voltage))
            {
                SetDisplayText(ERROR_VOLTAGE_MESSAGE);
                SetAlarm(false);
            }
            else
            {
                float status = ToPercentVoltage(voltage);
                if (status > 100f)
                    status = 100f;
                string strStatus = GetStrStatus(status);
                SetDisplayText(strStatus);
                SetAlarm(IsAlarmLvl(status));
            }
        }

        private void SetDisplayText(string text)
        {
            displayTextField.text = text;
        }

        private void SetAlarm(bool active)
        {
            if (active)
            {
                displayTextField.text += ALARM_MARK;
                displayTextField.color = Color.red;
            }
            else
            {
                displayTextField.color = Color.white;
            }
        }

        private bool IsAlarmLvl(float status)
        {
            return status <= alarmVoltageLvl;
        }

        private float ToPercentVoltage(float voltage)
        {
            return 100f * (voltage - minBatteryVoltage) / (maxBatteryVoltage - minBatteryVoltage);
        }

        private float ParseVoltage(string strVoltage)
        {
            float voltage = 0f;
            if (float.TryParse(strVoltage, out voltage))
            {
                return voltage;
            }
            else
            {
                return float.NaN;
            }
        }

        private string GetStrStatus(float status)
        {
            string strStatus = status.ToString(PERCENT_VOLTAGE_FORMAT);
            return strStatus + PERCENT_SIGN;
        }

        private string GetStrVoltage()
        {
            return sourceBatteryPanel.text;
        }
    }
}






