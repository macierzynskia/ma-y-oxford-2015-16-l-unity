using UnityEngine;
using System.Collections;
using System;

    public class ErrorBehaviour : MonoBehaviour
    {
        string errorOccureTime = "2015-08-20T14:35:00.00";
        GameObject dataSource = null;

        string
            startTime,
            stopTime;

        // Use this for initialization
        void Start()
        {
            CreateStartTime();
            CreateStopTime();
        }

        string GetErrorOcureTime()
        {
            return null;
        }

        void CreateStartTime()
        {
            DateTime start = DateTime.Parse(errorOccureTime).AddSeconds(-10.0);
            TimeConverter conv = new TimeConverter(start);
            startTime = conv.Convert();
        }

        void CreateStopTime()
        {
            DateTime stop = DateTime.Parse(errorOccureTime).AddSeconds(10.0);
            TimeConverter conv = new TimeConverter(stop);
            stopTime = conv.Convert();
        }

        public void SetDataSource()
        {
            dataSource = gameObject;
        }

        public string StartTime
        {
            get
            {
                return startTime;
            }
        }

        public string StopTime
        {
            get
            {
                return stopTime;
            }
        }

        public GameObject DataSource
        {
            get
            {
                return dataSource;
            }
            set
            {
                dataSource = value;
            }
        }

        public string ErrorOccureTime
        {
            get
            {
                return errorOccureTime;
            }
            set
            {
                errorOccureTime = value;
                CreateStartTime();
                CreateStopTime();
            }
        }
    }
