﻿using UnityEngine;
using System.Collections;

public abstract class HistoricalDataGetter : MonoBehaviour {

    protected string        ip;
    protected string 		port        = "8080";
    protected string 		endPoint;
    protected string 		url;
	protected string 		startTime;
	protected string 		stopTime;

	protected WWW 			www;

	protected RecievedData 	data;

	public GameObject 
		player,
		loadingGIF,
		historicalPanel;

	public string Ip {
		get {
			return ip;
		}
		set {
			ip = value;
		}
	}
	
	public RecievedData GetData(){
		//Debug.Log (data.Count);
		return data;
	}

	protected abstract IEnumerator DownloadData();
	public abstract void ControllView();
	public abstract void RunPlayer ();
}
