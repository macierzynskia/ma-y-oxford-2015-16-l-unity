using UnityEngine;
using System.Collections;
using SimpleJSON;
using System;



    public class IoTTGetterFromError : IoTTHistoricalDataGetter
    {

        private string errorUrl;

        public GameObject errorNotification;
        public IoTTSensorAll iottSensors;

        private string lastTimestamp = "";
        private string currentTimestamp = "";

        void Start()
        {
            StartCoroutine(UpdateErrorTimestamp());
        }

        public void SetUrl(string ip)
        {
            if (string.IsNullOrEmpty(currentTimestamp))
            {
                endPoint = "/alertMessages/search/findByThingId%7B?thingId=LegoRobot";
                errorUrl = "http://" + ip + ":" + port + endPoint;
            }
            else
            {
                endPoint = "/alertMessages/search/findByThingIdBetweenTimestamp/?thingId=LegoRobot&dateFrom=";
                errorUrl = "http://" + ip + ":" + port + endPoint + ConvertToMillis(currentTimestamp).ToString()
                    + "&dateTo=" + ConvertToMillis(DateTimeOffset.Now.ToString()).ToString();
            }
            Debug.Log(errorUrl);
        }

        public override void RunPlayer()
        {
            errorNotification.GetComponent<ErrorBehaviour>().ErrorOccureTime = currentTimestamp;
            startTime = errorNotification.GetComponent<ErrorBehaviour>().StartTime;
            stopTime = errorNotification.GetComponent<ErrorBehaviour>().StopTime;

            Debug.Log(startTime);
            Debug.Log(stopTime);

            StartCoroutine(DownloadData());
        }

        public override void ControllView()
        {
            if (data.Count == 0)
            {
                errorNotification.SetActive(true);
                Debug.Log("There is no data!");
            }
            else
            {
                errorNotification.SetActive(false);
                iottSensors.turnOff();
                player.SetActive(true);
                lastTimestamp = currentTimestamp;
            }
        }

        private IEnumerator UpdateErrorTimestamp()
        {
            float waitTime = 2f;
            while (true)
            {
                SetUrl(ip);
                WWW www = new WWW(errorUrl);
                float updateTime = Time.time;
                yield return www;
                updateTime = Time.time - updateTime >= 0.0 ? Time.time - updateTime : 0.0f;
                yield return new WaitForSeconds(waitTime - updateTime);
                if (string.IsNullOrEmpty(www.error))
                {
                    JSONNode answer = JSON.Parse(www.text);
                    DateTimeOffset timestamp = GetNearestTimestamp(answer["_embedded"]["alertMessages"]);
                    if (!timestamp.Equals(DateTimeOffset.MinValue))
                    {
                        currentTimestamp = TimeConverter.Convert(timestamp.LocalDateTime.AddHours(0));
                        if (!currentTimestamp.Equals(lastTimestamp))
                        {
                            errorNotification.GetComponent<ErrorBehaviour>().ErrorOccureTime = currentTimestamp;
                            errorNotification.SetActive(true);
                        }
                    }
                }
            }
        }

        private DateTimeOffset GetNearestTimestamp(JSONNode answer)
        {
            DateTimeOffset nearest = DateTimeOffset.MinValue;
            if (answer[0]["detectionTimestamp"] != null)
            {
                double seconds = double.Parse(answer[0]["detectionTimestamp"]);
                nearest = new DateTimeOffset(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(seconds));
            }
            return nearest;
        }

        private double ConvertToMillis(string timestamp)
        {
            DateTime current = DateTimeOffset.Parse(timestamp).DateTime;
            current = current.AddHours(-2);
            return (current - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
        }

    }
























