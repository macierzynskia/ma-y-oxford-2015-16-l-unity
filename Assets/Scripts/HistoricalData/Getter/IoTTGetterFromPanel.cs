using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.Collections.Generic;

public class IoTTGetterFromPanel : IoTTHistoricalDataGetter {

	public override void RunPlayer(){
		startTime = historicalPanel.transform.FindChild ("Confirm").GetComponent<DateCreator> ().GetStartDate ();
		stopTime  = historicalPanel.transform.FindChild ("Confirm").GetComponent<DateCreator> ().GetStopDate ();
		
		Debug.Log (startTime);
		Debug.Log (stopTime);
		
		StartCoroutine(	DownloadData ());
	}

	public override void ControllView (){
		if (data.Count == 0) {
			historicalPanel.SetActive(true);
			Debug.Log ("There is no data in IoTT!");
		}
		else{
			historicalPanel.SetActive(false);
			player.SetActive (true);
		}
	}

	public void StopHistoricalPanel(){
		StopCoroutine (DownloadData ());
		data = new RecievedData ();
		loadingGIF.SetActive (false);
	}
}
