using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.Collections.Generic;

public class IoTTHistoricalDataGetter : HistoricalDataGetter{

	protected void SetUrl(){
		Debug.Log ("IP z setUrl() z IoTT: " + ip);
		url = "http://" + ip + ":" + port + "/measurementMessages/" +
			"search/findByThingIdAndDateRange/?sourceThingId=LegoRobot&" +
				"dateFrom=" + startTime + "&dateTo=" + stopTime;
	}

	protected override IEnumerator DownloadData(){
		SetUrl ();
		Debug.Log ("URL z IOTTHistoricalDataGetter: "+url);
		loadingGIF.SetActive (true);
		www = new WWW (url);
		yield return www;
		loadingGIF.SetActive (false);
		if (string.IsNullOrEmpty(www.error)) {
			Debug.Log ("Download from IoTT done");
			JSONNode restAnswer = JSON.Parse (www.text);
			int i = 0;
			string key;
			List<string> value = new List<string>();
			data = new RecievedData();

			while(restAnswer["_embedded"]["measurementMessages"][i]["receivedTimestamp"]!=null){
				value = new List<string>();
				key = restAnswer["_embedded"]["measurementMessages"][i]["receivedTimestamp"];
				value.Add(restAnswer["_embedded"]["measurementMessages"][i]["measurements"]["gyro"]);
				value.Add(restAnswer["_embedded"]["measurementMessages"][i]["measurements"]["battery"]);
	            value.Add(restAnswer["_embedded"]["measurementMessages"][i]["measurements"]["rotateMotor"]);
	            value.Add(restAnswer["_embedded"]["measurementMessages"][i]["measurements"]["liftMotor"]);
	            value.Add(restAnswer["_embedded"]["measurementMessages"][i]["measurements"]["handMotor"]);
	            value.Add(restAnswer["_embedded"]["measurementMessages"][i]["measurements"]["color"]);
	            value.Add(restAnswer["_embedded"]["measurementMessages"][i]["measurements"]["distance"]);
				data.Add(key,value);
				key = null;
				value = null;
				i++;
			}
			ControllView();
		} 
	}

	public override void ControllView (){
	}

	public override void RunPlayer (){
	}
}
