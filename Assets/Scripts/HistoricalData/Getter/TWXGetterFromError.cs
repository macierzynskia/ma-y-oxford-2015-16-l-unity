﻿using UnityEngine;
using System.Collections;
using System;
using SimpleJSON;
using System.Collections.Generic;
using System.Text;

    public class TWXGetterFromError : TWXHistoricalDataGetter
    {

        private string errorUrl;
        private string errorEndPoint;

        public GameObject errorNotification;
        public ThingWorxSensorAll twxSensorAll;

        public string currentTimestamp = "";
        private string lastTimestamp = "";
        DateTime errorTimestamp;

        void Start()
        {
            errorEndPoint = "/Thingworx/Things/LegoRobotArmErrorTable/Services/GetLastErrorDate";
            postHeader = new Dictionary<string, string>();
            postHeader.Add("Content-Type", "application/json");
            postHeader.Add("Accept", "application/json");
            postHeader.Add("appKey", "b046a65c-ba57-4c5e-b580-5ef9c75fb89b");
            Debug.Log("TWX Error Thread Started!");

            StartCoroutine(UpdateErrorTimestamp());
        }

        /// <summary>
        /// You can set IP for TWX server, to correctly send request for errors.
        /// </summary>
        /// <param name="ip">Server's IP. For example 192.168.215.215 or localhost. 
        /// Local network hostname(ex. L0267) is not acceptable.</param>
        public void SetUrl(string ip)
        {
            base.Ip = ip;
            errorUrl = "http://" + ip + ":" + port + errorEndPoint;
            Debug.Log("URL for TWX ERROR GETTER: " + errorUrl);
        }

        /// <summary>This method sets error occure time in <see cref="ErrorBehaviour"/>. 
        ///  Then gets start and stop time to download data from server.
        /// </summary>
        public override void RunPlayer()
        {
            errorNotification.GetComponent<ErrorBehaviour>().ErrorOccureTime = errorTimestamp.ToString();

            startTime = errorNotification.GetComponent<ErrorBehaviour>().StartTime;
            stopTime = errorNotification.GetComponent<ErrorBehaviour>().StopTime;

            Debug.Log("Error from TWX. Start time: " + startTime);
            Debug.Log("Error from TWX. Stop time: " + stopTime);

            StartCoroutine(DownloadData());
        }

        public override void ControllView()
        {
            if (data.Count == 0)
            {
                errorNotification.SetActive(true);
                Debug.Log("There is no data from TWX!");
            }
            else
            {
                errorNotification.SetActive(false);
                twxSensorAll.turnOff();
                player.SetActive(true);
            }
        }

        private IEnumerator UpdateErrorTimestamp()
        {
            SetUrl(ip);
            float waitTime = 2f;
            while (true)
            {
                WWW www;
                var encoding = new UTF8Encoding();
                string emptyPost = " ";
                www = new WWW(errorUrl, encoding.GetBytes(emptyPost), postHeader);
                yield return www;

                float updateTime = Time.time;
                updateTime = Time.time - updateTime >= 0.0 ? Time.time - updateTime : 0.0f;
                yield return new WaitForSeconds(waitTime - updateTime);

                if (string.IsNullOrEmpty(www.error))
                {
                    JSONNode restAnswer = JSON.Parse(www.text);

                    currentTimestamp = restAnswer["rows"][0]["result"];
                    if (lastTimestamp != currentTimestamp)
                    {
                        lastTimestamp = currentTimestamp;   
                        errorTimestamp = DateTime.Parse(currentTimestamp).AddHours(1);

                        Debug.Log("Error timestamp: " + errorTimestamp.ToString());

                        errorNotification.GetComponent<ErrorBehaviour>().ErrorOccureTime = errorTimestamp.ToString();
                        errorNotification.SetActive(true);
                    }                 
                }
            }
        }
    } 

