﻿using UnityEngine;
using System.Collections;

public class TWXGetterFromPanel : TWXHistoricalDataGetter {

	public override void RunPlayer(){
		startTime = historicalPanel.transform.FindChild ("Confirm").GetComponent<DateCreator> ().GetStartDate ();
		stopTime  = historicalPanel.transform.FindChild ("Confirm").GetComponent<DateCreator> ().GetStopDate ();
		
		Debug.Log ("StartTime from Historical Panel: " + startTime);
		Debug.Log ("StartTime from Historical Panel: " + stopTime);
		
		StartCoroutine(	DownloadData ());
	}
	
	public override void ControllView (){
		if (data.Count == 0) {
			historicalPanel.SetActive(true);
			Debug.Log ("There is no data in TWX!");
		}
		else{
			historicalPanel.SetActive(false);
			player.SetActive (true);
		}
	}
	
	public void StopHistoricalPanel(){
		StopCoroutine (DownloadData ());
		data = new RecievedData ();
		loadingGIF.SetActive (false);
	}
}
