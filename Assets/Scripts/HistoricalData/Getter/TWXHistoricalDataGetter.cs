﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using SimpleJSON;

public class TWXHistoricalDataGetter : HistoricalDataGetter {

	public Dictionary<string, string> postHeader;

	void Init(){
		endPoint = "/Thingworx/Things/LegoRobotArmStream/Services/GetHistoricalDataChart";
		postHeader = new Dictionary<string, string> ();
		postHeader.Add ("Content-Type", "application/json");
		postHeader.Add ("appKey", "b046a65c-ba57-4c5e-b580-5ef9c75fb89b");
		postHeader.Add ("Accept", "application/json");
	}

	public void SetUrl(){
        Init();
		startTime = "?startDate=" + startTime;
		stopTime = "&stopDate=" + stopTime;
		url = "http://" + ip + ":" + port + endPoint + startTime + stopTime;
		Debug.Log ("Url z TWXHistoricalDataGetter: " + url); 
	}
	
	protected override IEnumerator DownloadData(){
		SetUrl ();

		loadingGIF.SetActive (true);

		var encoding = new UTF8Encoding ();
		string emptyPost = " ";
		www = new WWW (url, encoding.GetBytes (emptyPost), postHeader);
		yield return www;
        float waitTime = 2f;
        float updateTime = Time.time;
        updateTime = Time.time - updateTime >= 0.0 ? Time.time - updateTime : 0.0f;
        yield return new WaitForSeconds(waitTime - updateTime);

        loadingGIF.SetActive (false);

		if (string.IsNullOrEmpty(www.error)) {
			Debug.Log ("Download from TWX done");

			JSONNode restAnswer = JSON.Parse (www.text);

			int i = 0;

			string key;
			List<string> value = new List<string>();
			data = new RecievedData();

			while(restAnswer["rows"][i]["date"]!=null){
				value = new List<string>();
				key = restAnswer["rows"][i]["date"];
				value.Add(restAnswer ["rows"][i]["gyroAngle"]);
				value.Add(restAnswer ["rows"][i]["voltage"]);
				value.Add(restAnswer ["rows"][i]["rotateMotorAngle"]);
				value.Add(restAnswer ["rows"][i]["liftMotorAngle"]);
				value.Add(restAnswer ["rows"][i]["handMotorAngle"]);
				value.Add(restAnswer ["rows"][i]["color"]);
				value.Add(restAnswer ["rows"][i]["distance"]);
				data.Add(key,value);
				key = null;
				value = null;
				i++;
			}
			ControllView();
		} 
	}

	public override void ControllView (){}

	public override void RunPlayer (){}
}
