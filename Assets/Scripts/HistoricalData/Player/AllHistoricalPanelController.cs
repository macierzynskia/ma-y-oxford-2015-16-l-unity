﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AllHistoricalPanelController : MonoBehaviour {

	public Button 						errorNotification;
	public Button 						taskNotification;
	public Button 						historicalDataButton;
    
	public PlaymodeController			playmodeController;

	public GameObject					toDoList;
	public GameObject					instructions;

	void Start () {
	}

	void Update () {
		UpdateButtonsState ();
	}

	private void UpdateButtonsState(){
		if (playmodeController.gameObject.activeSelf) {
			errorNotification.interactable 		= false;
			taskNotification.interactable 		= false;
			historicalDataButton.interactable 	= false;
		} else {
			errorNotification.interactable 		= true;
			if (!toDoList.activeSelf && !instructions.activeSelf)
				taskNotification.interactable 	= true;
			historicalDataButton.interactable 	= true;
		}
	}
}
