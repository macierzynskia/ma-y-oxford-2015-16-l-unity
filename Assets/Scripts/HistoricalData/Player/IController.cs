﻿using System.Collections;

public interface IController{

	void UpdateLabels ();
	
	void ExitPlayer ();
	
	void InitPlayer();
	
	IEnumerator PlayDataSet();
}
