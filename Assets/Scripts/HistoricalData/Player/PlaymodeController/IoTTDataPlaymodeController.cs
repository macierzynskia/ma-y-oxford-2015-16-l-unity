using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine.UI;

public class IoTTDataPlaymodeController : PlaymodeController {

	protected override void GetData(){
		dataSource = error.GetComponent<ErrorBehaviour> ().DataSource;
		if (dataSource == error) {
			data = main.GetComponent<IoTTGetterFromError> ().GetData ();
			warning.SetActive(true);
			Debug.Log ("Data from IoTT Error Notification");
		} else {
			data = historicalData.GetComponent<IoTTGetterFromPanel> ().GetData ();
			warning.SetActive(false);
			Debug.Log ("Data from IoTT Historical Data Panel");
		}
		error.GetComponent<ErrorBehaviour> ().DataSource = null;
	}
}
