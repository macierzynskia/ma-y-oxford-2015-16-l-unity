using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public abstract class PlaymodeController : MonoBehaviour{

	protected GameObject 
		playButton,
		pauseButton,
		playSlider;
	
	protected bool 
		isInitialized = false,
		isInCoroutine = false,
		isPlaying;

    public WholeListSizeBehaviour wholeListSizeBehaviour;
	
	public Text 
		currentTimeLabel,
		actualSpeedLabel,
		gyroValue,
		batteryValue,
		rotateMotorValue,
		liftMotorValue,
		handMotorValue,
		colorValue,
		distanceValue;

    public GameObject
        error,
        historicalData,
        main,
        warning;
	//	replayModeTextPanel;

	public int actualPlayingIndex;
	
	public double actualPlayingSpeed = 0;

	protected List<string> 				dataValuesSet;
	protected RecievedData 				data;
	protected IoTTHistoricalDataGetter 	iottDataGetter;
	protected TWXHistoricalDataGetter	twxDataGetter;
	protected TimeConverter 			converter;
	protected GameObject 				dataSource;
	
	public const int 	CHANGING_SPEED_FACTOR = 2;
	public const double DEFAULT_PLAYING_SPEED = 10;

	void Start () {
		Debug.Log ("First DataPlayer init");
		InitPlayer ();
	}
	
	void Update () {
		if (!isInCoroutine && isPlaying) {
			StartCoroutine (PlayDataSet ());
			isInCoroutine = true;
		}

		if (!isInitialized) 
			InitPlayer();
	}

	public void ChangePlaymodeState(){
		isPlaying = !isPlaying;
		if (isPlaying)
			PlayDataGraphicControll ();
		else
			PauseDataGraphicControll ();
	}
	
	public void UpdateActualPlayingIndex(){
		actualPlayingIndex= (int)playSlider.GetComponent<Slider> ().value;
	}
	
	public void IncreaseActualSpeed(){
		if (actualPlayingSpeed < 400)
			actualPlayingSpeed *= CHANGING_SPEED_FACTOR;
		Debug.Log ("Actual speed increased to: " + actualPlayingSpeed);
	}
	
	public void DecreaseActualSpeed(){
		if (actualPlayingSpeed > 0.25) 
			actualPlayingSpeed /= CHANGING_SPEED_FACTOR;
		Debug.Log ("Actual speed decreased to: " + actualPlayingSpeed);
	}

	void OnEnable(){
		//replayModeTextPanel.SetActive (true);
        wholeListSizeBehaviour.setBigPosition(515);
        wholeListSizeBehaviour.setSmallPosition(325);
    }
	
	void OnDisable(){
		//replayModeTextPanel.SetActive (false);
        wholeListSizeBehaviour.setBigPosition(265);
        wholeListSizeBehaviour.setSmallPosition(85);
    }
	
	protected void PlayDataGraphicControll(){
		playButton.GetComponent<RawImage> ().enabled = false;
		pauseButton.GetComponent<RawImage> ().enabled = true;
		Debug.Log ("Stop request");
	}
	
	protected void PauseDataGraphicControll(){
		playButton.GetComponent<RawImage> ().enabled = true;
		pauseButton.GetComponent<RawImage> ().enabled = false;
		Debug.Log ("Start request");
	}

	public void UpdateLabels(){
		if (data != null) {
			actualSpeedLabel.text = actualPlayingSpeed.ToString ();
			converter = new TimeConverter((string)data.GetKeyList() [actualPlayingIndex]);
			currentTimeLabel.text = converter.Convert().Replace("T", " ");
			
			dataValuesSet = (List<string>)data.GetValueList() [actualPlayingIndex];		//
			gyroValue.text = dataValuesSet [0];
			batteryValue.text = dataValuesSet [1];
			rotateMotorValue.text = dataValuesSet [2];
			liftMotorValue.text = dataValuesSet [3];
			handMotorValue.text = dataValuesSet [4];
			colorValue.text = dataValuesSet [5];
			distanceValue.text = dataValuesSet [6];
			
			dataValuesSet = null;
		}
	}	
	
	public void ExitPlayer(){
		//Debug.Log ("Exit Player request");

		if (isPlaying)
			ChangePlaymodeState ();
		
		actualPlayingSpeed = DEFAULT_PLAYING_SPEED;
		actualPlayingIndex = 0;
		isInCoroutine = false;
		isInitialized = false;
		data = null;
		dataValuesSet = null;
		dataSource = null;
		currentTimeLabel.text = "";
		
		playSlider.GetComponent<Slider> ().maxValue = 0;
	}

	public void InitPlayer(){
		GetData ();
		Debug.Log ("Data package size from TWX:" + data.Count);
		if (!isInitialized) {
			playButton = GameObject.Find ("PlayImage");
			pauseButton = GameObject.Find ("PauseImage");
			playSlider = GameObject.Find ("PlayingSlider");
			actualPlayingSpeed = DEFAULT_PLAYING_SPEED;
			isPlaying = false;
			if (data != null) {
				playSlider.GetComponent<Slider> ().maxValue = data.Count - 1;
				isInitialized = true;
			}
		}
	}
	
	protected IEnumerator PlayDataSet(){
		UpdateLabels ();
		playSlider.GetComponent<Slider> ().value = actualPlayingIndex;
		yield return new WaitForSeconds (1/(float)actualPlayingSpeed);
		if (actualPlayingIndex == data.Count - 1) {
			ChangePlaymodeState ();
			playSlider.GetComponent<Slider> ().value = 0;
		}
		if (actualPlayingIndex < data.Count-1) 
			actualPlayingIndex++;
		isInCoroutine = false;
	}

	protected abstract void GetData();
}
