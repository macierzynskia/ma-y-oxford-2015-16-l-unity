﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class TWXDataPlaymodeController : PlaymodeController {
	
	protected override void GetData(){
		dataSource = error.GetComponent<ErrorBehaviour> ().DataSource;
		if (dataSource == error) {
			data = main.GetComponent<TWXGetterFromError> ().GetData ();
			warning.SetActive(true);
			Debug.Log ("Data from TWX Error Notification");
		} else {
			data = historicalData.GetComponent<TWXGetterFromPanel> ().GetData ();
			warning.SetActive(false);
			Debug.Log ("Data from TWX Historical Data Panel");
		}
		error.GetComponent<ErrorBehaviour> ().DataSource = null;
	}
}
