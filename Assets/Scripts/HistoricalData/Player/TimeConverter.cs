﻿using UnityEngine;
using System.Collections;
using System;

public class TimeConverter{

	private string inputTime;
	private string dateFormat = "yyyy-MM-ddTHH:mm:ss.ff";

	public TimeConverter(string inputTime){
		this.inputTime = inputTime;
	}

	public TimeConverter(DateTime inputTime){
		this.inputTime = inputTime.ToString ();
	}

	public string Convert(){
		return DateTime.Parse(inputTime).ToString(dateFormat);
	}

	public static string Convert(DateTime time){
		return new TimeConverter (time).Convert ();
	}
}
