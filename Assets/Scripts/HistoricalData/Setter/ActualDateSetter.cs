﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ActualDateSetter : MonoBehaviour{
	
	public Text
		minute,
		hour,
		day,
		month,
		year;

	public void SetDate(){
		minute.text = DateTime.UtcNow.Minute.ToString();
		KeepingTwoDigits (Convert.ToInt32 (minute.text), minute);

		DateTime dt = DateTime.UtcNow.AddHours (1);
		hour.text = dt.Hour.ToString ();
		KeepingTwoDigits (Convert.ToInt32 (hour.text), hour);

		day.text 	= DateTime.UtcNow.Day.ToString();
		KeepingTwoDigits (Convert.ToInt32 (day.text), day);

		month.text 	= DateTime.UtcNow.Month.ToString();
		KeepingTwoDigits (Convert.ToInt32 (month.text), month);

		year.text 	= DateTime.UtcNow.Year.ToString();
	}

	private void KeepingTwoDigits(int value, Text t){
		if (value < 10) 
			t.text = t.text.Insert(0,"0");
	}
}
