﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DateChoiceController : MonoBehaviour {

	public Text text;
	public int 
		max,
		min;
	public bool isDayField;
	private Text 
		lengthOfMonthControler0,
		lengthOfMonthControler1;
	private int 
		month,
		year;

	void Start () {
		if (isDayField) 
		{
			lengthOfMonthControler0 = GameObject.Find ("Month").GetComponent<Text> ();
			lengthOfMonthControler1 = GameObject.Find ("Year").GetComponent<Text> ();
		}
	}

	void Update () {
		if (isDayField) 
		{
			int.TryParse(lengthOfMonthControler0.text,out month);
			int.TryParse(lengthOfMonthControler1.text,out year);
			switch(month){
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					max = 31;
					break;
				case 4:
				case 6:
				case 9:
				case 11:
					max = 30;
					break;
				case 2:
					if(year%4==0)
						max = 29;
					else
						max = 28;
					break;
				}
			Refresh();
		}
	}
	private void Refresh(){
		int value = int.Parse (text.text);
		if (value > max) 
			value = max;
		text.text = value.ToString();
		KeepingTwoDigits (value);
	}

	public void Increase (){
		int value = int.Parse (text.text);
		value++;
		if (value > max) 
			value = min;
		text.text = value.ToString();
		KeepingTwoDigits (value);
	}

	public void Decrease (){
		int value = int.Parse (text.text);
		value--;
		if (value < min) 
			value = max;
		text.text = value.ToString();
		KeepingTwoDigits (value);
	}

	private void KeepingTwoDigits(int value){
		if (value < 10) 
			text.text = text.text.Insert(0,"0");
	}
}
