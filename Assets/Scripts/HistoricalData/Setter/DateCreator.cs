﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class DateCreator : MonoBehaviour {

	private string 
		year,
		month,
		day,
		hour,
		minute;

	private string 
		startDate,
		stopDate,
		duration;
    
	public GameObject slider;

    public GameObject
        yearObject,
        monthObject,
        dayObject,
        hourObject,
        minuteObject;


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		year 	= yearObject.GetComponent<Text> ().text;
		month	= monthObject.GetComponent<Text> ().text;
		day 	= dayObject.GetComponent<Text> ().text;
		hour 	= hourObject.GetComponent<Text> ().text;
		minute 	= minuteObject.GetComponent<Text> ().text;
	}

	public void CreateStartDate(){
		startDate = year + "-" + month + "-" + day + "T" + hour + ":" + minute;
		DateTime dt = DateTime.Parse (startDate);
		TimeConverter time = new TimeConverter (dt);
		startDate = time.Convert ().ToString();
	}

	public void CreateStopDate(){
		switch ((int)slider.GetComponent<Slider>().value) 
		{
		case 0:
			stopDate = DateTime.Parse(startDate).AddSeconds(10).ToString();
			break;
		case 1:
			stopDate = DateTime.Parse(startDate).AddSeconds(30).ToString();
			break;
		case 2:
			stopDate = DateTime.Parse(startDate).AddMinutes(1).ToString();
			break;
		case 3:
			stopDate = DateTime.Parse(startDate).AddMinutes(2).ToString();
			break;
		case 4:
			stopDate = DateTime.Parse(startDate).AddMinutes(5).ToString();
			break;
		case 5:
			stopDate = DateTime.Parse(startDate).AddMinutes(10).ToString();
			break;
		}

		TimeConverter time = new TimeConverter (stopDate);
		stopDate = time.Convert ();
	}

	public string GetStartDate(){
		return startDate;
	}

	public string GetStopDate(){
		return stopDate;
	}
}
