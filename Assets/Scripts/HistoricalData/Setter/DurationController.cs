﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DurationController : MonoBehaviour {

	public Text 	infoText;
	public Slider 	durationSlider;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		switch ((int)durationSlider.value) 
		{
		case 0:
			infoText.text = "10 seconds";
			break;
		case 1:
			infoText.text = "30 seconds";
			break;
		case 2:
			infoText.text = "1 minute";
			break;
		case 3:
			infoText.text = "2 minutes";
			break;
		case 4:
			infoText.text = "5 minutes";
			break;
		case 5:
			infoText.text = "10 minutes";
			break;
		}
	}
}
