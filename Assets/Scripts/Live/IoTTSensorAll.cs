﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System;

public class IoTTSensorAll : SensorAll {

	void Start () {
		ip  = "iott-core.tt.com.pl";
		port = "8083";
		endPoint = "/lastMeasurementMessages/LegoRobot";
		isActive = true;
		SetUrl (ip);
	}

	void Update () {
		if (!isInCoroutine && isActive) {
			StartCoroutine (GetAllSensor());
			isInCoroutine = true;
		}
	}

	public void SetUrl(string ip){ 
		this.ip = ip;
		url = "http://" + ip + ":" + port + endPoint;
	}

	protected override IEnumerator GetAllSensor ()
	{
		www = new WWW (url);
		yield return www;
		if (string.IsNullOrEmpty(www.error)) {
			JSONNode restAnswer = JSON.Parse (www.text);
			gyro.text 		= restAnswer["measurements"]["gyro"];
			distance.text 	= restAnswer["measurements"]["distance"];
			battery.text	= restAnswer["measurements"]["battery"];
			lift.text 		= restAnswer["measurements"]["liftMotor"];
			rotate.text 	= restAnswer["measurements"]["rotateMotor"];
			hand.text 		= restAnswer["measurements"]["handMotor"];
			color.text 		= restAnswer["measurements"]["color"];
		} 
		else 
		{
			gyro.text 		= "Error";
			distance.text 	= "Error";
			battery.text 	= "Error";
			lift.text 		= "Error";
			rotate.text		= "Error";
			hand.text 		= "Error";
			color.text 		= "Error";
		}
		isInCoroutine = false;
	}
}