﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class SensorAll : MonoBehaviour {

	public string 		ip;
	protected string 	endPoint;
	public string 		url;
	public string		port;

	protected bool 
		isInCoroutine = false,
		errorOccured;

	public bool isActive = true;

	public Text 
		gyro,
		distance,
		color,
		hand,
		lift,
		rotate,
		battery;

	public Sprite
		errorLabel,
		normalLabel;

	protected WWW www;

	public void turnOff(){
		isActive = false;
		isInCoroutine = false;
	}

	public void turnOn(){
		isActive = true;
		isInCoroutine = false;
	}

	public void ChangeActivity(){
		isActive = !isActive;
	}

	protected abstract IEnumerator GetAllSensor ();
}