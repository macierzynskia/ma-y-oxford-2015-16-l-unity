using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.Collections.Generic;
using AssemblyCSharp;
using UnityEngine.UI;
using System.Text;

public class ThingWorxSensorAll : SensorAll {

	Dictionary<string, string> postHeader;

	 void Start(){
		ip  = "192.168.156.43";
		port = "8080";
		endPoint = "/Thingworx/Things/RemoteLegoRobotArm/Services/GetPropertyValues";
		isActive = true;
		SetUrl (ip);

		postHeader = new Dictionary<string, string> ();
		postHeader.Add ("Content-Type", "application/json");
		postHeader.Add ("AppKey", "b046a65c-ba57-4c5e-b580-5ef9c75fb89b");
		postHeader.Add ("Accept", "application/json");
	}

	 void Update () {
		if (!isInCoroutine && isActive) {
			StartCoroutine (GetAllSensor());
			isInCoroutine = true;
		}
	}

	public void SetUrl(string ip){
		this.ip = ip;
		url = "http://" + ip + ":"+ port + endPoint;

	}

	protected override IEnumerator GetAllSensor(){
		var encoding = new UTF8Encoding ();
		string emptyPost = " ";
		www = new WWW (url, encoding.GetBytes (emptyPost), postHeader);
		yield return www;
		if (string.IsNullOrEmpty(www.error)) {
			JSONNode restAnswer = JSON.Parse (www.text);
			gyro.text		= restAnswer ["rows"][0]["gyroAngle"];
			distance.text 	= restAnswer ["rows"][0]["distance"];
			battery.text 	= restAnswer ["rows"][0]["voltage"];
			lift.text 		= restAnswer ["rows"][0]["liftMotorAngle"];
			rotate.text 	= restAnswer ["rows"][0]["rotateMotorAngle"];
			hand.text 		= restAnswer ["rows"][0]["handMotorAngle"];
			color.text 		= restAnswer ["rows"][0]["color"];
		} 
		else 
		{
			gyro.text 		= "Error";
			distance.text 	= "Error";
			battery.text 	= "Error";
			lift.text 		= "Error";
			rotate.text 	= "Error";
			hand.text 		= "Error";
			color.text 		= "Error";
		}
		isInCoroutine = false;
	}
}
