using System;
namespace AssemblyCSharp
{
	public class Note
	{
		private string noteText 	= "";
		private string noteOrigin 	= "";

		public Note (){
		}

		public Note(string noteText){
			this.noteText = noteText;
		}

		public Note(string noteText, string noteOrigin){
			this.noteText = noteText;
			this.noteOrigin = noteOrigin;
		}

		public void setNoteOrigin(string origin){
			noteOrigin = origin;
		}

		public void setNoteText(string text){
			this.noteText = text;
		}

		public string getNoteText(){
			return noteText;
		}

		public string getNoteOrigin(){
			return noteOrigin;
		}
	}
}

