﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using AssemblyCSharp;

public class NoteButtonContainer : MonoBehaviour {

	public string uniqueNotePanelName = "NotePanel";

	private GameObject noteListPanel;
	private NoteListBehaviour noteList;
	private GameObject parentObject;
	//private string headerName = "Header";
	
	private List<Note> notes = new List<Note>();

	// Use this for initialization
	void Start () {
		NotePanelRefs refs = GameObject.Find (uniqueNotePanelName).GetComponent<NotePanelRefs>();
		noteListPanel = refs.noteListPanel;
		noteList = refs.noteList;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void activateNotes(){
		noteListPanel.SetActive (true);
		noteList.setNotes (notes, transform.parent.name);
	}
}
