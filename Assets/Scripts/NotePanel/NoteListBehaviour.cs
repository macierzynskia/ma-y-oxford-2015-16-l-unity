﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using AssemblyCSharp;

public class NoteListBehaviour : MonoBehaviour {

	[SerializeField]Transform noteListField; 
	[SerializeField]GameObject noteButton;

	public NoteListPanelBehaviour noteListPanelBehaviour;
	public Text header;
	public int notePreviewLength = 30;

	Vector3 scale = new Vector3(1.0f, 1.0f, 1.0f);

	private List<GameObject> buttons = new List<GameObject>();
	private List<Note> notes = new List<Note> ();
	private Note currentNote = null;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void setNotes(List<Note> notes, string originName){
		this.notes = notes;
		header.text = originName;
		updateNotes ();
	}

	public void addCreatedNote(Note note){
		notes.Add (note);
	}

	public void removeCurrentNote(){
		if (currentNote != null) {
			notes.Remove(currentNote);
			if(notes.Count > 0)
				currentNote = notes[0];
			else
				currentNote = null;
			updateNotes();
		}
	}

	public void updateNotes(){
		noteButton.SetActive (true);
		foreach (GameObject g in buttons) {
			Destroy(g);
		}
		buttons.Clear ();
		foreach (Note note in notes) {
			GameObject button = (GameObject)Instantiate(noteButton);
			button.name = "NoteButton";
			button.transform.SetParent(noteListField);
			button.GetComponent<RectTransform>().localScale = scale;
			string textNote = note.getNoteText();
			string abrev = textNote.Substring(0, notePreviewLength > textNote.Length ? textNote.Length : notePreviewLength);
			if(abrev.Length < textNote.Length)
				abrev += "...";
			button.GetComponentInChildren<Text>().text = abrev;
			button.GetComponent<Button>().onClick.AddListener(()=>{
				updateCurrentNote(button);
			});
			buttons.Add(button);
		}
		noteButton.SetActive (false);
	}
	
	public Note generateNewNote(){
		Note note = new Note("", header.text);
		currentNote = note;
		updateNotes ();
		return note;
	}

	public Note getCurrentNote(){
		return currentNote;
	}

	private void updateCurrentNote(GameObject currentButton){
		int index = buttons.IndexOf (currentButton);
		if (index >= 0) {
			currentNote = notes[index];
		}
	}
}



















