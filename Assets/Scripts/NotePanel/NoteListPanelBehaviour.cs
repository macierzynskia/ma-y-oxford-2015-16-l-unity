using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class NoteListPanelBehaviour : MonoBehaviour {

	public TwxNoteSender 		twxNoteSender;
	public IoTTNoteSender		iotNoteSender;
	public NoteListBehaviour 	noteList;

	public void addNote(){
		twxNoteSender.gameObject.SetActive (true);
		twxNoteSender.SetTextNote (noteList.generateNewNote ());
	}

	public void addCreatedNote(Note note){
		noteList.addCreatedNote (note);
	}

	public void openNote(){
		Note currentNote = noteList.getCurrentNote ();
		if (currentNote != null) {
			twxNoteSender.gameObject.SetActive (true);
			twxNoteSender.SetTextNote (currentNote);
		}
	}

	public void removeCurrentNote(){
		noteList.removeCurrentNote ();
	}

	public void updateNoteList(){
		noteList.updateNotes ();
	}
}
