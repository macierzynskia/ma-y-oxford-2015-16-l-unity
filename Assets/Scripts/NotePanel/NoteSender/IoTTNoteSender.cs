﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using System.Text;
using System.Collections.Generic;

public class IoTTNoteSender : NoteTextSetter {
	
	void Start () {
		ip 			= "iott-core.tt.com.pl";
		port 		= "8080";
		endPoint 	= "serviceNote";
		url 		= "http://" + ip + ":" + port + endPoint;
	}

	protected string AsJson(Note note){
		string json = "{\"thingId\":\"LegoRobot\",\"sensor\":\"";
		json += note.getNoteOrigin () + "\",\"text\":\"";
		json += note.getNoteText () + "\"}";
		return json;
	}

	public override void SetPostUrl(){
		url = "http://" + ip + ":" + port + endPoint;	
	}

	protected override IEnumerator PostCreatedNote(){
		SetPostUrl ();

		string jsonString = AsJson (currentNote);

		Debug.Log ("JSON string from IoTT NotePost: " + jsonString);
		var encoding = new UTF8Encoding ();

		Dictionary<string, string> postHeader = new Dictionary<string, string> ();
		postHeader.Add ("Content-Type", "application/json");

		Debug.Log("Note to IoTT post URL: " + url);
		WWW request = new WWW (url, encoding.GetBytes (jsonString), postHeader);

		yield return request;
		Debug.Log ("After posting to IoTT!");
	}
}
