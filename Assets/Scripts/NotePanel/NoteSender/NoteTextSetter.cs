﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using AssemblyCSharp;
using System.Text;
using System.Collections.Generic;

public abstract class NoteTextSetter : MonoBehaviour {

	public Text 					textField;
	public InputField 				inputField;
	public NoteListPanelBehaviour 	notePanel;				 

	protected bool	newNote;
	protected Note 	currentNote;

	protected string 	ip;
	protected string 	port;
	protected string	url;
	protected string	endPoint;

	public string Ip {
		get {
			return ip;
		}
		set {
			ip = value;
		}
	}

	public void SetTextNote(Note note){
		currentNote = note;
		inputField.text = string.Copy (note.getNoteText ());
		newNote = string.IsNullOrEmpty (note.getNoteText ());
	}

	public void onSave(){
		if (!string.IsNullOrEmpty (inputField.text)) {
			currentNote.setNoteText (inputField.text);
			if(newNote){
				notePanel.addCreatedNote(currentNote);
				StartCoroutine(PostCreatedNote());
			}
			notePanel.updateNoteList ();
		}
		onExit ();
	}

	public void onCancel(){
		notePanel.updateNoteList ();
		gameObject.SetActive (false);
	}

	public void onExit(){
		onCancel ();
	}

	public abstract void SetPostUrl();
	protected abstract IEnumerator PostCreatedNote();
}

















