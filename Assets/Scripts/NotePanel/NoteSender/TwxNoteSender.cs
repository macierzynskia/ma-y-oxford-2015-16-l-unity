﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using AssemblyCSharp;
using System;
using SimpleJSON;

public class TwxNoteSender : NoteTextSetter {

	private StringBuilder urlParams;
	private string note;

	void Start () {
		//ip 			= "192.168.156.60"; //IP Piotrka z 23.09.2015
		port 		= "8080";
		endPoint 	= "/Thingworx/Things/LegoRobotArmServiceNotesDataTable/Services/AddSimple";
		url 		= "http://" + base.ip + ":" + port + endPoint;
	}

	private void CreateURLNote(){
		note = currentNote.getNoteText ().Replace (" ", "%20");
		Debug.Log (note);
	}

	private void UpdateParams(){
		CreateURLNote ();
		urlParams = new StringBuilder ();
		urlParams.Append ("?note=");
		urlParams.Append (note);
		urlParams.Append ("&notifier=");
		urlParams.Append ("Serviceman1");
		urlParams.Append ("&date=");
		urlParams.Append (DateTime.UtcNow.ToString().Replace(" ", "%20"));
		urlParams.Append ("&source=");
		urlParams.Append (currentNote.getNoteOrigin());
	}

	public override void SetPostUrl ()
	{
		UpdateParams ();
		url = "http://" + base.ip + ":" + port + endPoint + urlParams;
		Debug.Log ("URL of NoteSender to TWX: " + url);
	}

	protected override IEnumerator PostCreatedNote ()
	{
		Debug.Log("Posting note to TWX...");
		SetPostUrl ();
		var encoding = new UTF8Encoding ();
		
		Dictionary<string, string> postHeader = new Dictionary<string, string> ();
		postHeader.Add ("Content-Type", "application/json");
		postHeader.Add ("appKey", "b046a65c-ba57-4c5e-b580-5ef9c75fb89b");
		postHeader.Add ("Accept", "application/json");

		string emptyPost = " ";
		WWW request2 = new WWW (url, encoding.GetBytes (emptyPost), postHeader);
		yield return request2;

		Debug.Log ("Posted to TWX!");
	}
}
