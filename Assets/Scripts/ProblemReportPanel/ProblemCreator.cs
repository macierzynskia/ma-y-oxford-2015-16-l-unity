﻿using UnityEngine;
using System.Collections;

public class ProblemCreator : MonoBehaviour {

    public Problem          problem;
    public GameObject       problemPanel;
    private string          problemInfo;

    public void UpdateProblemInfo()
    {
        Debug.Log("Problem Created");
        problem             = problemPanel.GetComponent<ProblemDescriptionCreator>().CreateRef();
        problem.name        = gameObject.GetComponent<PartProperties>().partName;
        problem.wtPartUfid  = gameObject.GetComponent<PartProperties>().wtPartUfid;
    }
}
