﻿using UnityEngine;
using UnityEngine.UI;

public class ProblemDescriptionCreator : MonoBehaviour {

    public Text textField;

    public Problem problem;

    public GameObject partID;
    public GameObject partName;

    void OnEnable() {
        UpdateHeaders();
    }

    private void UpdateHeaders()
    {
        partID.GetComponent<Text>().text    = problem.wtPartUfid;
        partName.GetComponent<Text>().text  = problem.name;
    }

    public Problem CreateRef()
    {  
        return problem = new Problem();               
    }

    public void UpdateContent() {
        problem.description = textField.text;
    }
}
