﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Text;
using System.Collections.Generic;

public class ProblemReporterBehaviour : MonoBehaviour {

    public    string ip         = "192.168.215.242";
    protected string port       = "8080";
    protected string url;
    protected string endPoint   = "/Thingworx/Things/ptc-windchill-demo-thing-simple/Services/CreateProblemReport";

    protected StringBuilder urlParams;

    private Problem         problem;
    public  GameObject      problemPanel;
    public  GameObject      problemTextField;

    public void UpdateReference() {
        problem = problemPanel.GetComponent<ProblemDescriptionCreator>().problem;
    }

    private void SetPostUrl()
    {
        CreateURL();
        UpdateURLParams();
        url = "http://" + ip + ":" + port + endPoint + urlParams;
        Debug.Log("URL POST PROBLEM REPORT: " + url);
    }

    public void CreateURL()
    {
        problem.description = problem.description.Replace(" ", "%20");
        problem.description = problem.description.Replace("\n", "%0A");
        problem.description = problem.description.Replace("&", "%26");
        problem.description = problem.description.Replace("?", "%3F");
        problem.description = problem.description.Replace("=", "%3D");
    }

    private StringBuilder UpdateURLParams() {
        urlParams = new StringBuilder();
        urlParams.Append("?name=");
        urlParams.Append(problem.name);
        urlParams.Append("&description=");
        urlParams.Append(problem.description);
        urlParams.Append("&wtPartUfid=");
        urlParams.Append(problem.wtPartUfid);
        return urlParams;
    }

    public void Send() {
        StartCoroutine(PostProblemReport());
    }

    protected IEnumerator PostProblemReport()
    {
        UpdateReference();
        Debug.Log("Posting problem report to TWX...");
        SetPostUrl();
        var encoding = new UTF8Encoding();

        Dictionary<string, string> postHeader = new Dictionary<string, string>();
        postHeader.Add("Content-Type", "application/json");
        postHeader.Add("appKey", "b046a65c-ba57-4c5e-b580-5ef9c75fb89b");
        postHeader.Add("Accept", "application/json");

        string emptyPost = " ";
        WWW www = new WWW(url, encoding.GetBytes(emptyPost), postHeader);
        yield return www;

        Debug.Log("Posted problem to TWX!");
    }

    public void Exit() {
        problemTextField.GetComponent<InputField>().text = "";
        problemPanel.SetActive(false);
        problemPanel.GetComponent<ProblemDescriptionCreator>().problem  = null;
        problemPanel.GetComponent<ProblemDescriptionCreator>().partID.GetComponent<Text>().text   = "";
        problemPanel.GetComponent<ProblemDescriptionCreator>().partName.GetComponent<Text>().text = "";        
    }
}
