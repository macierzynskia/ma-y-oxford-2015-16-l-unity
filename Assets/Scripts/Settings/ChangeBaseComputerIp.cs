using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class ChangeBaseComputerIp : MonoBehaviour
{

    public InputField inputField;

    public IoTTNoteSender iottNoteSender;
    public TwxNoteSender twxNoteSender;

    public IoTTSensorAll iottSensorAll;
    public ThingWorxSensorAll thingWorxSensorAll;

    public IoTTGetterFromError iottGetterFromError;
    public IoTTGetterFromPanel iottGetterFromPanel;

    public TWXGetterFromError twxGetterFromError;
    public TWXGetterFromPanel twxGetterFromPanel;

    public TreeListBehaviour treeListBehaviour;

    public ProblemReporterBehaviour problemReporterBehaviour;

    public HistoricalDataGetter historicalDataGetter;

    public TicketDataGetter ticketDataGetter;
    public FeedbackPanelController feedbackPanelController;

    void Start() {
        if (PlayerPrefs.HasKey("0")) {
            inputField.text = PlayerPrefs.GetString("0");
            Debug.Log("IP changed from PlayerPrefs to : " + inputField.text);
            UpdateUrlForTWX();
            gameObject.SetActive(false);
        }
    }

    public void UpdateUrlForIoTT()
    {
        Debug.Log(inputField.text);

        iottNoteSender.Ip = inputField.text;

        iottSensorAll.SetUrl(inputField.text);
        iottGetterFromError.Ip = inputField.text;
        iottGetterFromPanel.Ip = inputField.text;
    }

    public void UpdateUrlForTWX()
    {          
        Debug.Log("New IP: " + inputField.text);
        PlayerPrefs.SetString("0", inputField.text);
        PlayerPrefs.Save();

        thingWorxSensorAll.SetUrl(inputField.text);
        treeListBehaviour.SetUrl(inputField.text);

        twxNoteSender.Ip = inputField.text;
        problemReporterBehaviour.ip = inputField.text;
            
        twxGetterFromPanel.Ip = inputField.text;
        twxGetterFromError.SetUrl(inputField.text);

        historicalDataGetter.Ip = inputField.text;

        ticketDataGetter.Ip = inputField.text;
        ticketDataGetter.SetTicketUrl();
        feedbackPanelController.ip = inputField.text;
    }
}
