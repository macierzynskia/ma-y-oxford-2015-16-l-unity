﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IpChange : MonoBehaviour {

	public ThingWorxSensorAll sensorAll;
	InputField inputField;
	public GameObject changeIpPanel;
	public ComboBox ipList;

	void Start () {
		inputField = changeIpPanel.GetComponentInChildren<InputField>();
		changeIpPanel.SetActive(false);
		PlayerPrefs.SetString ("0", "http://localhost:9090");
		PlayerPrefs.SetString("1","http://192.168.1.100:9090");
		refreshIpList ();
	}

	public void ChangeIp(){
		sensorAll.ip = ipList.header.textComponent.text;
	}

	public void AddNewAddress(){
		bool isOnList = true;
		bool isNew = true;
		string key="0";
		int keyCounter = 0;
		while (isOnList) 
		{
			key = keyCounter.ToString();
			isOnList = PlayerPrefs.HasKey(key);
			keyCounter++;
			if(inputField.text.Equals(PlayerPrefs.GetString(key)))
				isNew=false;
		}
		if(isNew)
		{
			PlayerPrefs.SetString (key, inputField.text);
			ipList.AddItem (PlayerPrefs.GetString (key));
			PlayerPrefs.Save();
		}
	}
	public void refreshIpList(){
		bool isOnList = true;
		string key="0";
		int keyCounter = 0;
		while (isOnList) 
		{
			key = keyCounter.ToString();
			isOnList = PlayerPrefs.HasKey(key);
			keyCounter++;
			if(isOnList)
				ipList.AddItem(PlayerPrefs.GetString(key));
		}
	}
}
