﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelStatus : MonoBehaviour {

	private const string panelSuffix 	= "Panel";
	private const string noteButtonName = "NoteButton";
	private const string textValueName 	= "ElementValue";

	public float		minValue;
	public float		maxValue;
	public string		errorMsg = "ERROR";

	public Sprite	normalBackground;
	public Sprite	disconnectedBackground;
	public Sprite	errorBackground;

	private GameObject	panel;
	private GameObject	noteButton;
	private Image		panelImageStatus;
	private Image		noteButtonImageStatus;
	private Text		textValue;
	
	void Start () {

		foreach (Transform child in transform) {
			if(child.name.EndsWith(panelSuffix)){
				panel = child.gameObject;
				break;
			}
		}

		noteButton = FindInChild (noteButtonName, panel.transform);
		textValue = FindInChild (textValueName, panel.transform).GetComponent<Text>();

		panelImageStatus = panel.GetComponent<Image> ();
		noteButtonImageStatus = noteButton.GetComponent<Image> ();
	}

	void Update () {
		updatePanelImageStatus ();
	}

	private void updatePanelImageStatus(){
		SetBackground (ChooseBackground ());
	}

	private void SetBackground(Sprite back){
		panelImageStatus.sprite = back;
		noteButtonImageStatus.sprite = back;
	}

	private float ParseFloat(string value){
		float parsed = 0f;
		if (float.TryParse (value, out parsed)) {
			return parsed;
		} else {
			return float.NaN;
		}
	}

	private string getStrFromPanel(){
		return textValue.text;
	}

	private Sprite ChooseBackground(){
		string strValue = getStrFromPanel ();
		float value = ParseFloat (strValue);
		if (strValue.Equals (errorMsg)) {
			return disconnectedBackground;
		} else if (float.IsNaN(value)) {
			return normalBackground;
		}else {
			if(value < minValue || value > maxValue){
				return errorBackground;
			}else{
				return normalBackground;
			}
		}
	}

	private GameObject FindInChild(string name, Transform transform){
		foreach (Transform child in transform) {
			if(child.name.Equals(name)){
				return child.gameObject;
			}
		}
		return null;
	}

}










