﻿using UnityEngine;
using System.Collections;

public class RotationWithCamera : MonoBehaviour {

	public GameObject rotatingObject;
	public GameObject baseOfLabel;

	void Start () {
		rotatingObject.transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward , baseOfLabel.transform.up);
	}

	void Update () {
		rotatingObject.transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward , baseOfLabel.transform.up);
	}
}