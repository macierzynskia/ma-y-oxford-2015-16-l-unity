﻿using UnityEngine;
using System.Collections;

public enum State {

	FIXED, 
	IN_PROGRESS, 
	OPEN, 
	SELECTED,

}
