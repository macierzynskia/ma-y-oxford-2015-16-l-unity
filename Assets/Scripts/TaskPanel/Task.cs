﻿using UnityEngine;
using System.Collections.Generic;

public class Task{

	public string name;
	List<string> instructionsSet;
	public State state;

	public Task(string name, List<string> inst){
		this.name = name;
		this.instructionsSet = inst;
		state = State.OPEN;
	}

	public List<string> GetInstructionsSet(){
		return instructionsSet;
	}

}
