using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;


public class TaskListBehaviour : MonoBehaviour {

	public List<Task> tasksList;


	[SerializeField]Transform tasksListField; 
	[SerializeField]GameObject taskButton;

	List<GameObject> buttonList;

	Vector3 scale = new Vector3(1.0f,1.0f,1.0f);
	//bool isInCoroutine = false;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.Find("TaskPanel");
		TaskPanelBehaviour taskPanel = (TaskPanelBehaviour) go.GetComponent(typeof(TaskPanelBehaviour));

		tasksList = taskPanel.setOfTasks;

		buttonList = new List<GameObject>();
	
		foreach(Task t in tasksList){
			GameObject button = (GameObject)Instantiate(taskButton);
			button.transform.SetParent(tasksListField);
			button.GetComponentInChildren<Text>().text = t.name;
			button.GetComponent<RectTransform>().localScale = scale;
			button.GetComponent<ContainTask>().setTask(t);
			Task capturedTask = t;
			button.GetComponent<Button>().onClick.AddListener (
				()=> {
					taskPanel.SetCurrentTask(capturedTask);
					Debug.Log(capturedTask.name);
				}
				);
			buttonList.Add(button);
		}
		taskButton.SetActive (false);
	}

	// Update is called once per frame
	void Update () {
		foreach (GameObject button in buttonList) 
		{
			Task t = button.GetComponent<ContainTask>().getTask();
			Text buttonTickText = button.transform.Find("TickText").GetComponent<Text>();
			Button buttonBody = button.GetComponent<Button>();
			if(t.state==State.OPEN){
				buttonBody.interactable = true;
				buttonTickText.text = "";
			}else{
				buttonBody.interactable = false;
				buttonTickText.text = "\u2713";
			}
		}
	}
}
