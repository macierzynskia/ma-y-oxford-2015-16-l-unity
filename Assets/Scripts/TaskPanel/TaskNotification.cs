﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TaskNotification : MonoBehaviour {

	bool isSomethingToComplete;

	public int amountOfTask = 0;

	GameObject 
		taskNotificationButton,
		completed,
		uncompleted,
		go;

	TaskPanelBehaviour taskPanel;
		
	// Use this for initialization
	void Start () {
		taskNotificationButton = GameObject.Find ("TaskNotification");
		completed = taskNotificationButton.transform.FindChild ("Completed").gameObject;
		uncompleted = taskNotificationButton.transform.FindChild ("Uncompleted").gameObject;

		go = GameObject.Find("TaskPanel");
		taskPanel = (TaskPanelBehaviour) go.GetComponent(typeof(TaskPanelBehaviour));
	}
	
	// Update is called once per frame
	void Update () {
		CheckTaskPanelStatus ();

		if (isSomethingToComplete) {
			if(amountOfTask <= 999){
				taskNotificationButton.transform.FindChild ("State").GetComponent<Text> ().text = amountOfTask.ToString ();
				completed.SetActive (false);
			}
			else{
				taskNotificationButton.transform.FindChild ("State").GetComponent<Text> ().text = "999+";
				completed.SetActive (false);
			}
		} else {
			taskNotificationButton.transform.FindChild ("State").GetComponent<Text> ().text = "\u2713";
			completed.SetActive (true);
		}
	}

	void CheckTaskPanelStatus(){
		amountOfTask = 0;
		foreach (Task t in taskPanel.setOfTasks)
			if(t.state == State.OPEN)
				amountOfTask++;

		if (amountOfTask > 0) {
			if(taskNotificationButton.GetComponent<Button>().interactable)
				uncompleted.transform.FindChild("NewTask").gameObject.SetActive(true);
			isSomethingToComplete = true;
		}
		else {
			isSomethingToComplete = false;
			uncompleted.transform.FindChild("NewTask").gameObject.SetActive(false);
		}
	}
}
