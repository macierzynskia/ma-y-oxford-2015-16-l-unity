using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TaskPanelBehaviour : MonoBehaviour {

	string taskState;

	int 
		stepsCount,
		actualStep = 1;

	bool isInCoroutine = false;

	public GameObject instructionsPanel,
					  toDoListPanel;
	GameObject previousButton,
			   nextButton,
	           finishButton;

	Task currentTask;

	public Text currentInstruction;
	public List<Task> setOfTasks = new List<Task>();

	void InitPanels(){
		instructionsPanel.SetActive (false);
		toDoListPanel.SetActive (false);
	}

	void Start () {
		CreateSetOfTasks();
		InitPanels ();
	}

	void Update () {
		if (!isInCoroutine) {
			StartCoroutine(GenerateTask());
			isInCoroutine=true;
		}
	}

	void CreateSetOfTasks(){
		List<string> instSet = new List<string> ();
		instSet.Add("Turn off brick");
		instSet.Add("Open battery door");
		instSet.Add("Change battery");
		instSet.Add("Close battery door");
		setOfTasks.Add(new Task("Replace battery",instSet));
		List<string> instSet1 = new List<string> ();
		instSet1.Add("Turn off brick");
		instSet1.Add("Disconnect cable from sensor");
		instSet1.Add("Change gyro sensor");
		instSet1.Add("Connect cable to sensor");
		setOfTasks.Add(new Task("Change Gyro Sensor",instSet1));
		List<string> instSet2 = new List<string> ();
		instSet2.Add("Connect brick to charger");
		setOfTasks.Add(new Task("Recharge battery",instSet2));
		List<string> instSet3 = new List<string> ();
		instSet3.Add("Turn off brick");
		instSet3.Add("Disconnect cabel from brick");
		instSet3.Add("Disconnect cabel from sensor");
		instSet3.Add("Connect cabel to brick");
		instSet3.Add("Connect cabel to sensor");
		setOfTasks.Add(new Task("Replace cabel",instSet3));
	}


	public void Fix(){
		if (currentTask != null) {
			toDoListPanel.SetActive (false);
			instructionsPanel.SetActive (true);
			previousButton = instructionsPanel.transform.FindChild ("Previous").gameObject;
			previousButton.SetActive (false);
			nextButton = instructionsPanel.transform.FindChild ("Next").gameObject;
			nextButton.SetActive (true);
			finishButton = instructionsPanel.transform.FindChild ("Finish").gameObject;
			finishButton.SetActive (false);

			instructionsPanel.transform.FindChild ("Header").GetComponent<Text> ().text = currentTask.name;

			stepsCount = currentTask.GetInstructionsSet ().Count;
			currentTask.state = State.IN_PROGRESS;
			UpdateInstructionsPanel (1);
		}
	}

	void UpdateInstructionsPanel(int actualStep){
		currentInstruction.text = currentTask.GetInstructionsSet()[actualStep-1];
		instructionsPanel.transform.FindChild ("Step").GetComponent<Text> ().text = actualStep.ToString () + "/" + stepsCount.ToString ();

		if(actualStep == stepsCount){
			instructionsPanel.transform.FindChild("Next").gameObject.SetActive(false);
			instructionsPanel.transform.FindChild("Finish").gameObject.SetActive(true);
		}
	}

	public void Next(){
		previousButton.SetActive (true);
		actualStep++;
		if (actualStep < stepsCount) {
			UpdateInstructionsPanel (actualStep);
		}
		else {
			UpdateInstructionsPanel (actualStep);
			instructionsPanel.transform.FindChild("Next").gameObject.SetActive(false);
			instructionsPanel.transform.FindChild("Finish").gameObject.SetActive(true);

		}
	}

	public void Previous(){
		actualStep--;
		if (actualStep > 1) {
			if (!instructionsPanel.transform.FindChild ("Next").gameObject.activeSelf) 
				instructionsPanel.transform.FindChild ("Next").gameObject.SetActive (true);
				instructionsPanel.transform.FindChild("Finish").gameObject.SetActive(false);
				UpdateInstructionsPanel (actualStep);
		} 
		else {
			if (actualStep <= 1){
				UpdateInstructionsPanel (actualStep);
				previousButton.SetActive (false);
			}
		}
	}

	public void Finish(){
		Exit ();
		currentTask.state = State.FIXED;
		currentTask = null;
		toDoListPanel.transform.FindChild ("CurrentTask").GetComponent<Text> ().text = "CURRENT TASK: ";
	}

	public void Exit(){
		currentTask.state = State.OPEN;
		if(instructionsPanel.activeSelf){
			instructionsPanel.SetActive(false);
			actualStep = 1;
		}
	}
	public void SetCurrentTask(Task currentTask){
		this.currentTask = currentTask;
		toDoListPanel.transform.FindChild ("CurrentTask").GetComponent<Text> ().text = "CURRENT TASK: " + currentTask.name + " "+ currentTask.state.ToString ();
		//Debug.Log (currentTask.name);
	}
	IEnumerator GenerateTask(){
		int rnd = Random.Range (1, 11);
		//Debug.Log(rnd.ToString());
		switch (rnd) {
		case 1:
			setOfTasks[0].state=State.OPEN;
			break;
		case 2:
			setOfTasks[1].state=State.OPEN;			
			break;
		case 3:
			setOfTasks[2].state=State.OPEN;			
			break;
		case 4:
			setOfTasks[3].state=State.OPEN;
			break;
		}
		yield return new WaitForSeconds (10f);
		isInCoroutine = false;
	}
}
