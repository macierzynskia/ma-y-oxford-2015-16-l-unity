﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text;
using System.Collections.Generic;

public class FeedbackPanelController : MonoBehaviour {

    public Toggle resolved;
    public Toggle unresolved;
    public GameObject solutionStepPanel;
    public TicketPanelController ticketPanelController;
    public TicketDataGetter ticketDataGetter;
    public InputField feedBackText;

    public GameObject loadingGif;

    public string ip = "192.168.215.242";


    protected string port = "8080";
    protected string url;
    protected string endPoint = "/Thingworx/Things/LegoRobotArmTicketsTable/Services/resolveIssue";

    protected StringBuilder urlParams;

    string feedback;

    private void SetPostUrl()
    {
        CreateURL();
        UpdateURLParams();
        url = "http://" + ip + ":" + port + endPoint + urlParams;
        Debug.Log("URL Service ticket resolve: " + url);
    }

    public void CreateURL()
    {
        feedback = feedBackText.text;
        feedback = feedback.Replace(" ", "%20");
        feedback = feedback.Replace("\n", "%0A");
        feedback = feedback.Replace("&", "%26");
        feedback = feedback.Replace("?", "%3F");
        feedback = feedback.Replace("=", "%3D");
    }

    private StringBuilder UpdateURLParams()
    {
        urlParams = new StringBuilder();
        urlParams.Append("?errorCode=");
        urlParams.Append(ticketPanelController.errorCode.ticketName);
        urlParams.Append("&feedback=");
        urlParams.Append(feedback);
        urlParams.Append("&close=");
        urlParams.Append(unresolved.isOn.ToString());
        return urlParams;
    }

    private IEnumerator PostTicketFeedback()
    {
        
        Debug.Log("Posting problem report to TWX...");
        SetPostUrl();
        var encoding = new UTF8Encoding();

        Dictionary<string, string> postHeader = new Dictionary<string, string>();
        postHeader.Add("Content-Type", "application/json");
        postHeader.Add("appKey", "b046a65c-ba57-4c5e-b580-5ef9c75fb89b");
        postHeader.Add("Accept", "application/json");

        if (resolved.isOn)
        {
            ticketPanelController.ticketList.Remove(ticketPanelController.errorCode);
            ticketDataGetter.listOfTickets.Remove(ticketPanelController.errorCode.ticketName);
            Debug.Log("Ticket deleted from TicketList " + ticketPanelController.errorCode.ticketName);
        }

        string emptyPost = " ";
        WWW www = new WWW(url, encoding.GetBytes(emptyPost), postHeader);
        loadingGif.SetActive(true);
        yield return www;

        if (string.IsNullOrEmpty(www.error))
        {
            yield return new WaitForSeconds(3f);
            ticketDataGetter.StartCoroutine(ticketDataGetter.GetTicketsFromTWX());
        }
        solutionStepPanel.SetActive(false);
        loadingGif.SetActive(false);
    }

    public void SendFeedback() {
        ticketDataGetter.StopAllCoroutines();
        StartCoroutine(PostTicketFeedback());            
    }
}
