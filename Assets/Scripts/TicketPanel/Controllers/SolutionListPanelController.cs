﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class SolutionListPanelController : MonoBehaviour {


    TicketTask task;

    public GameObject solutionStepsPanel;
    public GameObject guiList;
    [SerializeField] GameObject solutionButton;
    Vector3 scaleVector = new Vector3(1, 1, 1);
    List<GameObject> buttonsList = new List<GameObject>();

    public void SetTask(TicketTask currentTask) {
        task = currentTask;
    }

	void Start () {
        RefreshList();
    }

    void OnEnable()
    {
        solutionButton.SetActive(false);
        RefreshList();
    }

    private void RefreshList()
    {
        foreach (GameObject button in buttonsList)
            Destroy(button);
        buttonsList.Clear();
        CreateButtonList();
    }

    void CreateButtonList() {
        foreach (Solution solution in task)
        {
            GameObject newSolution = (GameObject)Instantiate(solutionButton);
            newSolution.name = solution.solutionName;
            newSolution.transform.SetParent(guiList.transform);
            newSolution.SetActive(true);
            newSolution.GetComponentInChildren<Text>().text = newSolution.name;
            newSolution.GetComponent<RectTransform>().localScale = scaleVector;
            AddListener(newSolution, solution);
            buttonsList.Add(newSolution);
        }
    }

    public void AddListener(GameObject button, Solution solution)
    {

        button.GetComponent<Button>().onClick.AddListener(delegate { openSolution(solution); });
    }

    void openSolution(Solution s) {
        solutionStepsPanel.GetComponent<StepListPanelController>().SetSolution(s);
        solutionStepsPanel.SetActive(true);
        gameObject.SetActive(false);
    }
}
