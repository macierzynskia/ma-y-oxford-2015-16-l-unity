﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StepListPanelController : MonoBehaviour {

    public GameObject feedbackPanel;
    public GameObject instructionField;
    public Text stepField;
    public Button previousButton;
    public Button nextButton;
    public Button doneButton;
    public Button returnButton;
    public Toggle showAnim;

    protected Solution solution;

    private int stepsAmount = 0;
    private int actualStep  = 1;
    

    // Use this for initialization
    void Start () {
           
	}

    void OnEnable() {
        stepsAmount = solution.Count;
        actualStep = 1;
        CreateStepByStepInterface();
    }

    public void SetSolution(Solution s) {
        solution = s;
    }

    public void NextStep()
    {
        actualStep++;
        UpdateStepFieldControl();
        UpdateStepDescription();
        UpdateButtonsState();
        TurnOffAnimations();
    }

    public void PreviousStep()
    {
        actualStep--;
        UpdateStepFieldControl();
        UpdateStepDescription();
        UpdateButtonsState();
        TurnOffAnimations();
    }

    public void ResolveTicket()
    {
        TurnOffAnimations();
        previousButton.gameObject.SetActive(false);
        nextButton.gameObject.SetActive(false);
        doneButton.gameObject.SetActive(false);

        instructionField.SetActive(false);
        feedbackPanel.SetActive(true);
    }

    public void MenageAnimation() {
        if (!showAnim.isOn)
            solution[actualStep-1].StopAnimation();
        else
            solution[actualStep-1].StartAnimation();
    }

    void TurnOffAnimations() {
        showAnim.isOn = false;
        foreach (SolutionStep s in solution)
            s.StopAnimation();
    }

    void CreateStepByStepInterface()
    {
        feedbackPanel.SetActive(false);
        instructionField.SetActive(true);
        
        UpdateStepDescription();
        UpdateStepFieldControl();
        UpdateButtonsState();
    }

    void UpdateStepFieldControl() {
        stepField.text = actualStep.ToString() + "/" + stepsAmount.ToString();
    }

    void UpdateStepDescription() {
        instructionField.GetComponentInChildren<Text>().text = solution[actualStep - 1].GetStepInstruction();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msgText">Feedback message</param>
    /// <param name="state">State of service</param>
    void SendFeedback(string msgText, string state) {

    }

    void UpdateButtonsState() {
        if (stepsAmount == 1) //When amount of steps is equal to 1, there is only DONE button.
        {
            returnButton.gameObject.SetActive(true);
            previousButton.gameObject.SetActive(false);
            nextButton.gameObject.SetActive(false);
            doneButton.gameObject.SetActive(true);
        }
        else if (actualStep == stepsAmount) //When we are on last step of few, we can see PREVIOUS and DONE button.
        {
            returnButton.gameObject.SetActive(false);
            previousButton.gameObject.SetActive(true);
            nextButton.gameObject.SetActive(false);
            doneButton.gameObject.SetActive(true);
        }
        else if(actualStep == 1) //When wa are on the first step and ther is more than 1 on the list, we can see only NEXT button.
        {
            returnButton.gameObject.SetActive(true);
            previousButton.gameObject.SetActive(false);
            nextButton.gameObject.SetActive(true);
            doneButton.gameObject.SetActive(false);
        }
        else // Any another situation (for ex. 2/10, 3/4 etc)
        {
            returnButton.gameObject.SetActive(false);
            previousButton.gameObject.SetActive(true);
            nextButton.gameObject.SetActive(true);
            doneButton.gameObject.SetActive(false);
        }
    } 
}
