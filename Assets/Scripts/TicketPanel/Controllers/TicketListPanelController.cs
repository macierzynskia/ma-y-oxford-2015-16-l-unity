﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class TicketListPanelController : MonoBehaviour {

    TicketList ticketList;
    public GameObject solutionPanel;
    public GameObject guiList;
    public TicketPanelController ticketPanelController;
    [SerializeField]GameObject ticketButton;
    List<GameObject> buttonsList = new List<GameObject>();
    Vector3 scaleVector = new Vector3(1, 1, 1);

    void Start()
    {
        RefreshList();
    }

    public void SetTicketList(TicketList ticketList) {
        this.ticketList = ticketList;
    }

    void OnEnable() {
        ticketButton.SetActive(false);
        RefreshList();
    }

    void CreateButtonList() {
        foreach (TicketTask ticket in ticketList)
        {
            GameObject newTicket = (GameObject)Instantiate(ticketButton);
            newTicket.name = ticket.ticketName;
            newTicket.transform.SetParent(guiList.transform);
            newTicket.SetActive(true);
            newTicket.GetComponentInChildren<Text>().text = newTicket.name;
            newTicket.GetComponent<RectTransform>().localScale = scaleVector;
            //Debug.Log("TicketListPanel button created : " + newTicket.name);
            AddListener(newTicket, ticket);
            buttonsList.Add(newTicket);
        }
    }

    public void AddListener(GameObject button, TicketTask ticket)
    {
        //Debug.Log("TicketListPanel button's listener created!");
        button.GetComponent<Button>().onClick.AddListener(delegate { OpenTicket(ticket); });
    }

    void OpenTicket(TicketTask t) {
        solutionPanel.GetComponent<SolutionListPanelController>().SetTask(t);
        solutionPanel.SetActive(true);
        gameObject.SetActive(false);
        ticketPanelController.errorCode = t;
    }

    void RefreshList()
    {
        foreach (GameObject button in buttonsList)
        {
            Destroy(button);
            //Debug.Log("ticket button destroyed" + button.ToString());
        }
        buttonsList.Clear();
        //Debug.Log("button list cleared");
        CreateButtonList();
    }
}
