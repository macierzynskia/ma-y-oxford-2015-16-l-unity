﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TicketNotification : MonoBehaviour {

    bool isSomethingToComplete;

    public int amountOfTickets = 0;

    GameObject
        ticketNotificationButton,
        completed,
        uncompleted,
        go;

    TicketPanelController ticketPanel;

    // Use this for initialization
    void Start () {
        ticketNotificationButton = GameObject.Find("TicketNotification");
        completed = ticketNotificationButton.transform.FindChild("Completed").gameObject;
        uncompleted = ticketNotificationButton.transform.FindChild("Uncompleted").gameObject;

        go = GameObject.Find("TicketPanel");
        ticketPanel = (TicketPanelController)go.GetComponent(typeof(TicketPanelController));
    }

    void Update()
    {
        CheckTaskPanelStatus();

        if (isSomethingToComplete)
        {
            if (amountOfTickets <= 999)
            {
                ticketNotificationButton.transform.FindChild("State").GetComponent<Text>().text = amountOfTickets.ToString();
                completed.SetActive(false);
            }
            else
            {
                ticketNotificationButton.transform.FindChild("State").GetComponent<Text>().text = "999+";
                completed.SetActive(false);
            }
        }
        else
        {
            ticketNotificationButton.transform.FindChild("State").GetComponent<Text>().text = "\u2713";
            completed.SetActive(true);
        }
    }

    void CheckTaskPanelStatus()
    {
        amountOfTickets = ticketPanel.ticketList.Count;

        if (amountOfTickets > 0)
        {
            if (ticketNotificationButton.GetComponent<Button>().interactable)
                uncompleted.transform.FindChild("NewTask").gameObject.SetActive(true);
            isSomethingToComplete = true;
        }
        else
        {
            isSomethingToComplete = false;
            uncompleted.transform.FindChild("NewTask").gameObject.SetActive(false);
        }
    }
}
