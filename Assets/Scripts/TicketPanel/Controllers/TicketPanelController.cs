﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TicketPanelController : MonoBehaviour {

    public TicketList ticketList;
    public GameObject arrowHand;
    public GameObject arrow;
    public GameObject distance;
    public GameObject distanceIn;
    public GameObject distanceOut;
    public GameObject cableIn;
    public GameObject cableOut;
    public GameObject brick;
    public GameObject ticketListPanel;

    public TicketTask errorCode;

    private Dictionary<string,AnimationParams> animationDictionary;

    void Start () {
        ticketList = new TicketList();
        //BuildLiveworxTicketList();
        animationDictionary = buildAnimationDictionary();
        //Debug.Log("Ticket list size: " + ticketList.Count);
        ticketListPanel.GetComponent<TicketListPanelController>().SetTicketList(ticketList);
    }

    public void BuildTicketFrom2JsonString(string faultCode, string verify, string resolve) {
        TicketTask ticket = new TicketTask(faultCode);
        verify = verify.Replace("<ol>", "");
        verify = verify.Replace("</ol>", "");
        verify = verify.Replace("-&gt;", "");
        verify = verify.Replace("</li>", "");
        string[] separator = { "<li>" };

        string[] verifyTable = verify.Split(separator, StringSplitOptions.RemoveEmptyEntries);
        // "<ol><li>Nothing upfront</li><li>Not dirty</li><li>Still bad values -&gt; resolve issue by replacing sensor</li></ol>"

        resolve = resolve.Replace("<ol>", "");
        resolve = resolve.Replace("</ol>", "");
        resolve = resolve.Replace("</li>", "");
        string[] resolveTable = resolve.Split(separator, StringSplitOptions.RemoveEmptyEntries);
        // "<ol><li>Turn off</li><li>unplug cable</li><li>unplug sensor</li><li>plug sensor</li><li>plug cable</li><li>turn on</li></ol>"

        for (int i = 0;i<verifyTable.Length;i++) {
            Solution solution = new Solution(verifyTable[i]);
            if (i < verifyTable.Length - 1)
            {
                AnimationParams animationParams;
                SolutionStep solutionStep;
                if (animationDictionary.TryGetValue(verifyTable[i], out animationParams))
                {
                    solutionStep = new SolutionStep(verifyTable[i], animationParams.animationName, animationParams.movingModel, animationParams.staticModel);
                }
                else {
                    solutionStep = new SolutionStep(verifyTable[i], null,null,null);
                }
                solution.Add(solutionStep);
            }
            else {
                for (int j = 0; j < resolveTable.Length; j++) {
                    SolutionStep solutionStep;
                    AnimationParams animationParams;
                    if (animationDictionary.TryGetValue(resolveTable[j], out animationParams))
                    {
                        solutionStep = new SolutionStep(resolveTable[j], animationParams.animationName, animationParams.movingModel, animationParams.staticModel);
                    }
                    else
                    {
                        solutionStep = new SolutionStep(resolveTable[j], null, null, null);
                    }
                    solution.Add(solutionStep);
                }
            }
            ticket.Add(solution);
        }
        ticketList.Add(ticket);
    }

    private class AnimationParams {
        public string animationName;
        public GameObject movingModel;
        public GameObject staticModel;
        public AnimationParams(string a, GameObject m, GameObject s) {
            animationName = a;
            movingModel = m;
            staticModel = s;
        }
    }

    

    private Dictionary<string, AnimationParams> buildAnimationDictionary() {
        Dictionary<string, AnimationParams> result = new Dictionary<string, AnimationParams>();
        result.Add("Check sensor light", new AnimationParams("CheckLight", arrow, distance));
        result.Add("Check nothing upfront", new AnimationParams("CheckDirty", arrow, distance));
        result.Add("Turn off", new AnimationParams("TurnOffBrickAnimation", arrow, brick));
        result.Add("Unplug cable", new AnimationParams("CableAnimation", cableOut, distance));
        result.Add("Unplug sensor", new AnimationParams("DistanceSensorAnimation", distanceOut, null));
        result.Add("Plug sensor", new AnimationParams("DistanceSensorBackwardsAnimation", distanceIn, null));
        result.Add("Plug cable", new AnimationParams("CableBackwardAnimation", cableIn, distance));
        result.Add("Turn on", new AnimationParams("TurnOnBrickAnimation", arrow, brick));
        return result;
    }
    //  "<ol><li>Turn off</li><li>unplug cable</li><li>unplug sensor</li><li>plug sensor</li><li>plug cable</li><li>turn on</li></ol>"
    // Update is called once per frame
    void Update () {
	
	}


}
