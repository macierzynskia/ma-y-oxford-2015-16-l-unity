﻿using UnityEngine;
using System.Text;
using System.Collections.Generic;
using SimpleJSON;
using System;
using System.Collections;

public class TicketDataGetter : MonoBehaviour {

    private string ip   = "192.168.215.185";
    private string port = "8080";

    private string ticketUrl;
    private string ticketEndPoint;

    private string solutionUrl;
    private string solutionEndPoint;
    private string solutionSKDCode;

    private Dictionary<string, string> postHeader;

    public TicketPanelController ticketPanelController;
    public List<string> listOfTickets = new List<string>();

    public string   stepsToVerify { get; private set; }
    public string   stepsToResolve { get; private set; }
    public float    waitUntilFeedbackSend { get; set; }

    public string Ip
    {
        get
        {
            return ip;
        }

        set
        {
            ip = value;
        }
    }

    // Use this for initialization
    void Start () {
        ticketEndPoint      = "/Thingworx/Things/LegoRobotArmTicketsTable/Services/GetDataTableEntries";
        solutionEndPoint    = "/Thingworx/Things/KMThing/Services/SearchFullText?search=";
       
        postHeader = new Dictionary<string, string>();
        postHeader.Add("Content-Type", "application/json");
        postHeader.Add("Accept", "application/json");
        postHeader.Add("appKey", "b046a65c-ba57-4c5e-b580-5ef9c75fb89b");
      
        StartCoroutine(GetTicketsFromTWX());
    }

	
	// Update is called once per frame
	void Update () {

	}


    public void SetTicketUrl()
    {
        ticketUrl = "http://" + Ip + ":" + port + ticketEndPoint;
        Debug.Log("URL from SetTicketUrl() from TWXa: " + ticketUrl);    
    }

    protected void SetSolutionUrl()
    {
        solutionUrl = "http://" + Ip + ":" + port + solutionEndPoint + solutionSKDCode;
        Debug.Log("URL from SetSolutionUrl() from TWXa: " + solutionUrl);
    }

    public IEnumerator GetTicketsFromTWX()
    {
        Debug.Log("TWX Ticket Thread Started!");
        SetTicketUrl();

        float waitTime = 2f;
        while (true)
        {
            WWW www;
            var encoding = new UTF8Encoding();
            string emptyPost = " ";
            www = new WWW(ticketUrl, encoding.GetBytes(emptyPost), postHeader);
            yield return www;

            float updateTime = Time.time;
            updateTime = Time.time - updateTime >= 0.0 ? Time.time - updateTime : 0.0f;
            yield return new WaitForSeconds(waitTime - updateTime);

            if (string.IsNullOrEmpty(www.error))
            {
                JSONNode restAnswer = JSON.Parse(www.text);

                int i = 0; 
                               
                while (restAnswer["rows"][i] != null)
                {
                    if ((Convert.ToBoolean(restAnswer["rows"][i]["isOpened"])) && !listOfTickets.Contains(restAnswer["rows"][i]["errorCode"]))
                    {
                        solutionSKDCode = restAnswer["rows"][i]["errorCode"];
                        listOfTickets.Add(solutionSKDCode);
                        Debug.Log("New Ticket recieved from TWX: " + solutionSKDCode);
                        if (solutionSKDCode.Equals("FC432"))
                            StartCoroutine(GetSolutionFromSKD(solutionSKDCode));                  
                    }
                    i++;
                }
            }
        }
    }
    

    private IEnumerator GetSolutionFromSKD(string faultCode)
    {
        SetSolutionUrl();

        WWW www;
        var encoding = new UTF8Encoding();
        string emptyPost = " ";
        www = new WWW(solutionUrl, encoding.GetBytes(emptyPost), postHeader);
        yield return www;

        if (string.IsNullOrEmpty(www.error))
        {
            JSONNode restAnswer = JSON.Parse(www.text);

            stepsToVerify = restAnswer["rows"][0]["results"]["rows"][0]["values"][8][0].Value;
            Debug.Log("Steps to verify " + faultCode + " issue: " + stepsToVerify);

            stepsToResolve = restAnswer["rows"][0]["results"]["rows"][0]["values"][3][0].Value;
            Debug.Log("Steps to resolve " + faultCode + " issue: " + stepsToResolve);
            ticketPanelController.BuildTicketFrom2JsonString(faultCode, stepsToVerify, stepsToResolve);
        }        
    }
}
