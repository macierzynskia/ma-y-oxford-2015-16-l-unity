﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Solution : List<SolutionStep> {

    public string solutionName;

    public Solution(string name)
    : base()
    {
        solutionName = name;
    }
}
