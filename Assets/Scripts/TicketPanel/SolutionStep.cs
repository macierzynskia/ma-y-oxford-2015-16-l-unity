﻿using UnityEngine;
using System.Collections;

public class SolutionStep{

    string instruction;
    string animationName;
    Vector3 initialPosition;
    GameObject movingModel;
    GameObject staticModel;

    public SolutionStep(string instruction, string animationName, GameObject movingModel, GameObject staticModel) {
        this.instruction = instruction;
        this.animationName = animationName;
        this.movingModel = movingModel;
        this.staticModel = staticModel;
    }

    public void StartAnimation()
    {
        if (staticModel != null)
        {
            staticModel.SetActive(true);
        }
        if (movingModel != null)
        {
            movingModel.SetActive(true);
            movingModel.GetComponent<Animator>().enabled = true;
            movingModel.GetComponent<Animator>().Play(animationName);
            initialPosition = movingModel.transform.localPosition;
        }
    }
    public void StopAnimation() {
        if (staticModel != null)
        {
            staticModel.SetActive(false);
        }
        if (movingModel != null)
        {
            if (!initialPosition.Equals(new Vector3(0f, 0f, 0f)))
            {
                movingModel.transform.localPosition = initialPosition;
            }
            movingModel.GetComponent<Animator>().enabled = false;
            movingModel.SetActive(false);
        }
    }
    public string GetStepInstruction() {
        return instruction;
            }
}
