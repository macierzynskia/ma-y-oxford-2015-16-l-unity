﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.IO;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Text;

public class TreeListBehaviour : MonoBehaviour {

	[SerializeField]GameObject parentButton;
	[SerializeField]GameObject simpleButton;
	[SerializeField]GameObject examplePanel;

	string jsonString;
	JSONNode jsonTree;
    public ArrayList panelsList;
    ArrayList treeButtonsList;
    ArrayList modelsList;
    Vector3 scaleVector = new Vector3(1, 1, 1);

    private string ip;
    private string port;
    private string endPoint;
    private bool isActive;
    private Dictionary<string, string> postHeader;
    private string url;
    private WWW www;
    private bool isInCoroutine;


    // Use this for initialization
    void Start () {
        ip = "L0627";
        panelsList = new ArrayList();
        treeButtonsList = new ArrayList();
        FindingModels();
        StartBuildingTree();
	}

    void OnDisable() {
        turnOffModels();
    }
    class TreeMenuButton {
        public GameObject button;
        public string parentName;
        public string myId;
        public TreeMenuButton(GameObject button, string parent, string id) {
            this.button = button;
            parentName = parent;
            myId = id;
        }
    }
    public void FindingModels() {
        modelsList = new ArrayList();
        modelsList.Add(GameObject.Find("Hand/Hand3DModel/OR:wt.part.WTPart:60393"));
        modelsList.Add(GameObject.Find("Hand/Hand3DModel/OR:wt.part.WTPart:67020"));
        modelsList.Add(GameObject.Find("Hand/Hand3DModel/OR:wt.part.WTPart:66979"));
        modelsList.Add(GameObject.Find("Base/OR:wt.part.WTPart:60273"));
        modelsList.Add(GameObject.Find("Base/Battery/Battery/OR:wt.part.WTPart:60177"));
        modelsList.Add(GameObject.Find("Base/DistanceSensor/OR:wt.part.WTPart:67102"));
        
        foreach (GameObject model in modelsList) {
            model.SetActive(false);
        }
    }

    public void turnOffModels() {
        foreach (GameObject model in modelsList)
        {
            model.SetActive(false);
        }
    }

    public void PutButtonsInPanels() {
        foreach (TreeMenuButton treeButton in treeButtonsList)
        {
            treeButton.button.transform.SetParent(gameObject.transform);
            foreach (GameObject panel in panelsList)
            {
                if (panel.name.Equals(treeButton.parentName))
                {
                    treeButton.button.transform.SetParent(panel.transform);
                    //Debug.Log(treeButton.myId+"button umieszczony w panelu "+panel.name);

                }              
            }
            foreach (GameObject panel in panelsList)
            {
                if (panel.name.Equals(treeButton.myId))
                {
                    panel.transform.SetParent(treeButton.button.transform.parent.transform);
                    //Debug.Log(panel.name + "panel umieszczony w panelu " + treeButton.button.transform.parent.name);
                    panel.GetComponent<RectTransform>().localScale = scaleVector;
                }
            }

                treeButton.button.GetComponent<RectTransform>().localScale = scaleVector;
        }
    }
    public void MatchButtonsToModels() {
        foreach (TreeMenuButton button in treeButtonsList) {
            foreach (GameObject model in modelsList) {
                if (button.button.name.Equals(model.name))
                    AddListener(button.button, model);               
            }
        }
    }
    public void AddListener(GameObject button, GameObject model) {
        button.GetComponent<Button>().onClick.AddListener(delegate { model.GetComponent<TreeViewPanelBehaviour>().changeState(); });
    }

    public void IpInit() {
        port        = "8080";
        endPoint    = "/Thingworx/Things/ptc-windchill-demo-thing-simple/Services/GetPartStructureByNumber?number=0000000114";
        isActive    = true;
        url         = "http://" + ip + ":" + port + endPoint;
        postHeader  = new Dictionary<string, string>();

        postHeader.Add("Content-Type", "application/json");
        postHeader.Add("AppKey", "b046a65c-ba57-4c5e-b580-5ef9c75fb89b");
        postHeader.Add("Accept", "application/json");
    }

    public void SetUrl(string ip)
    {
        this.ip = ip;
        url = "http://" + ip + ":" + port + endPoint;
        StartBuildingTree();
    }

    protected IEnumerator GetTwxAnswer()
    {
        var encoding = new UTF8Encoding();
        string emptyPost = " ";
        Debug.Log("URL for POST on parts list: " + url);
        www = new WWW(url, encoding.GetBytes(emptyPost), postHeader);
        yield return www;
        if (string.IsNullOrEmpty(www.error))
        {
            jsonString = www.text;
            Debug.Log("REST answer from URL POST for part list: " + www.text);
            jsonTree = GetJsonTree();
            DeleteOldTree();
            BuildTreeMenu();
        }
        else
        {
            Debug.Log(www.error);
            jsonString = "";
        }
        isInCoroutine = false;
    }

    public void DeleteOldTree() {
        foreach (TreeMenuButton treeButton in treeButtonsList)
        {
            Destroy(treeButton.button);
        }
        foreach (GameObject panel in panelsList)
        {
            Destroy(panel);
        }
        treeButtonsList = new ArrayList();
        panelsList = new ArrayList();
    }

    private void StartBuildingTree()
    {
        IpInit();
        GetJsonString();
    }
    private JSONNode GetJsonTree()
    {
        JSONNode answer = JSONNode.Parse(jsonString);
        return answer;
    }
    private void GetJsonString()
    {
        if (!isInCoroutine && isActive)
        {
            StartCoroutine(GetTwxAnswer());
            isInCoroutine = true;
        }
    }
    private void BuildTreeMenu()
    {
        parentButton.SetActive(true);
        simpleButton.SetActive(true);
        examplePanel.SetActive(true);

        if (jsonTree != null)
        {
            int i = 0;
            while (jsonTree["rows"][i] != null)
            {
                GameObject newButton;
                if (jsonTree["rows"][i]["hasChildren"].AsBool == true)
                {
                    GameObject button = (GameObject)Instantiate(parentButton);
                    newButton = button;
                    SetLinkedStrings(newButton, i);

                    GameObject newPanel = CreateSubPanel(jsonTree["rows"][i]["treeId"].Value);
                    newPanel.GetComponent<RectTransform>().localScale = scaleVector;
                    newButton.GetComponent<Button>().onClick.AddListener(delegate { newPanel.GetComponent<TreeViewPanelBehaviour>().changeState(); });
                    newButton.GetComponentInChildren<TreeViewArrowBehaviour>().childPanel = newPanel;
                }
                else
                {
                    GameObject button = (GameObject)Instantiate(simpleButton);
                    newButton = button;
                    SetLinkedStrings(newButton, i);
                }

                TreeMenuButton newTreeButton = new TreeMenuButton(newButton, jsonTree["rows"][i]["parentId"].Value, jsonTree["rows"][i]["treeId"]);
                treeButtonsList.Add(newTreeButton);
                i++;
            }
            PutButtonsInPanels();
            MatchButtonsToModels();
        }
        foreach (GameObject panel in panelsList)
        {
            panel.SetActive(false);
        }
        parentButton.SetActive(false);
        simpleButton.SetActive(false);
        examplePanel.SetActive(false);
    }

    private void SetLinkedStrings(GameObject newButton, int i)
    {
        newButton.name = jsonTree["rows"][i]["objectId"];
        setPartProperties(newButton, jsonTree["rows"][i]["name"].Value, newButton.name);
        string name = jsonTree["rows"][i]["number"].Value + ", " +
            jsonTree["rows"][i]["name"].Value + ", " +
            jsonTree["rows"][i]["version"].Value;
        newButton.GetComponentInChildren<Text>().text = name;
    }

    private GameObject CreateSubPanel(string panelName)
    {
        GameObject newPanel = (GameObject)Instantiate(examplePanel);
        newPanel.name = panelName;

        panelsList.Add(newPanel);
        return newPanel;
    }

    private void setPartProperties(GameObject button, string name, string wtPartUfid)
    {
        button.GetComponentInChildren<PartProperties>().partName = name;
        button.GetComponentInChildren<PartProperties>().wtPartUfid = wtPartUfid;
    }
}

