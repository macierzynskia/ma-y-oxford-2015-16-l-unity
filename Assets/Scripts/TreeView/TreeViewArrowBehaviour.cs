﻿using UnityEngine;
using System.Collections;

public class TreeViewArrowBehaviour : MonoBehaviour {

	public GameObject childPanel;
	bool isDown=false;

	// Use this for initialization
	void Start () {
        //gameObject.GetComponent<RectTransform>().Rotate(0.0f, 0.0f, 90.0f);
    }
	
	// Update is called once per frame
	void Update () {
		if (childPanel.activeSelf&&!isDown) {
			gameObject.GetComponent<RectTransform>().Rotate(0.0f,0.0f,-90.0f);
            isDown = true;

		} 
		if (!childPanel.activeSelf&&isDown) {
			gameObject.GetComponent<RectTransform>().Rotate(0.0f,0.0f,90.0f);
			isDown = false;

        }
	}
}
