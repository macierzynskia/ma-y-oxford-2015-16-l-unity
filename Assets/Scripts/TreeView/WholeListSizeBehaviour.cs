﻿using UnityEngine;
using System.Collections;

public class WholeListSizeBehaviour : MonoBehaviour {
    bool big = false;
    int bigPosition =265;
    int smallPosition = 85;
    public TreeListBehaviour treeListBehaviour;
    ArrayList panelList;

    // Use this for initialization
    void Start () {
        panelList = treeListBehaviour.panelsList;
	}
	
	// Update is called once per frame
	void Update () {
        panelList = treeListBehaviour.panelsList;
        big = false;
        foreach (GameObject panel in panelList)
        {
            if (panel.name.Equals("/"))
            {
                if (panel.activeSelf)
                {
                    big = true;
                }
            }
        }
        changeSize();
    }

    public void changeSize() {
        if (big) {
            gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(980, 480);
            gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(-527, bigPosition);
        }
        else {
            gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(980, 70);
            gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(-527, smallPosition);
        }
    }
    public void setBigPosition(int yPosition)
    {
        bigPosition = yPosition;
    }
    public void setSmallPosition(int yPosition)
    {
        smallPosition = yPosition;
    }
}
